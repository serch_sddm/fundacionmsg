package com.sduran.fundacionmsg.activities;

import android.app.DialogFragment;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sduran.fundacionmsg.R;
import com.sduran.fundacionmsg.adapter.InvestmentArrayAdapter;
import com.sduran.fundacionmsg.adapter.MortgageArrayAdapter;
import com.sduran.fundacionmsg.helpers.InvestmentsReport;
import com.sduran.fundacionmsg.helpers.MortgagesReport;
import com.sduran.fundacionmsg.models.Investment;
import com.sduran.fundacionmsg.models.Mortgage;

import java.util.ArrayList;
import java.util.List;

public class ManageMortgageActivity extends AppCompatActivity {

    public static final String MY_TAG = ManageMortgageActivity.class.getSimpleName();
    Toolbar mToolbar;
    ListView mListView;
    LinearLayout mLoadingForm;
    TextView mEmpty;
    MortgageArrayAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_mortgage);
        setToolbar();


        mLoadingForm = (LinearLayout) findViewById(R.id.empty_form);
        mEmpty = (TextView) findViewById(R.id.no_mortgages_label);

        mListView = (ListView) findViewById(R.id.lv_mortgage);
        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListView.setMultiChoiceModeListener(new MyMultiClickListener());
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /**
                 * If they click an investment,
                 */
                Mortgage mortgageEntry = (Mortgage) mAdapter.getItem(position);
                editMortgageEntry(mortgageEntry);

            }
        });

        updateMortgages();

    }

    private void updateMortgages() {
        mLoadingForm.setVisibility(View.VISIBLE);
        mEmpty.setVisibility(View.GONE);
        mListView.setVisibility(View.GONE);
        new QueryForMortgagesTask().execute();
    }

    class QueryForMortgagesTask extends AsyncTask<Void, Void, List<Mortgage>> {

        @Override
        protected List<Mortgage> doInBackground(Void... unused) {
            List<Mortgage> mortgages = null;
            mortgages = MortgagesReport.getReport(getFilesDir());
            return mortgages;
        }

        @Override
        protected void onPostExecute(List<Mortgage> result) {
            super.onPostExecute(result);
            if (result == null) {
                mEmpty.setText("Failed loading, result is null");
                return;
            }

            if (result.isEmpty()) {
                Log.d(MY_TAG, "result is empty");
                result = new ArrayList<Mortgage>();
            }

            mAdapter = new MortgageArrayAdapter(
                    ManageMortgageActivity.this, android.R.layout.simple_list_item_activated_1,
                    result);
            mListView.setAdapter(mAdapter);


            mLoadingForm.setVisibility(View.GONE);

            if (mAdapter.isEmpty()) {
                mEmpty.setVisibility(View.VISIBLE);
            } else {
                mListView.setVisibility(View.VISIBLE);
            }

        }
    }

    private void editMortgageEntry(final Mortgage mortgageEntry) {
        DialogFragment df = new DialogFragment() {
            @Override
            public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                int style = android.support.v4.app.DialogFragment.STYLE_NORMAL, theme = R.style.MyDialogTheme;
                setStyle(style, theme);
            }

            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
                View view = inflater.inflate(R.layout.dialog_edit_mortgage_entry, container);
                getDialog().setTitle(getString(R.string.dialog_edit_mortgage_entry_title));

                // Change the title and divider
                //http://stackoverflow.com/questions/23255776/android-alert-dialog-replace-default-blue-with-another-color
                //http://stackoverflow.com/questions/23023877/how-to-change-appcompat-dialog-title-and-title-divider-color
                //http://developer.android.com/intl/es/reference/android/app/DialogFragment.html
                final Resources res = getResources();

                final int titleDividerId = res.getIdentifier("titleDivider", "id", "android");
                final int titleId = res.getIdentifier("alertTitle", "id", "android");
                final View titleDivider = getDialog().findViewById(titleDividerId);
                final View title = getDialog().findViewById(titleId);
                if (titleDivider != null) {
                    titleDivider.setBackgroundColor(res.getColor(R.color.accentColor));
                }
                if (title != null) {
                    ((TextView) title).setTextColor(res.getColor(R.color.accentColor));
                }

                final Button confirmButton = (Button) view
                        .findViewById(R.id.dialog_edit_mortgage_ok);
                final Button cancelButton = (Button) view
                        .findViewById(R.id.dialog_edit_mortgage_cancel);
                final EditText mortgagePremiumEditText = (EditText) view
                        .findViewById(R.id.dialog_edit_mortgage_premium);
                final EditText mortgageNameEditText = (EditText) view
                        .findViewById(R.id.dialog_edit_mortgage_name);
                final EditText mortgagePriceEditText = (EditText) view
                        .findViewById(R.id.dialog_edit_mortgage_price);
                final EditText mortgageIssuedEditText = (EditText) view
                        .findViewById(R.id.dialog_edit_mortgage_issued);
                final EditText mortgageIncomeEditText = (EditText) view
                        .findViewById(R.id.dialog_edit_mortgage_income);
                final EditText mortgageDateEditText = (EditText) view
                        .findViewById(R.id.dialog_edit_mortgage_date);
                final EditText mortgageTaxEditText = (EditText) view
                        .findViewById(R.id.dialog_edit_mortgage_tax);
                final EditText mortgageBalanceEditText = (EditText) view
                        .findViewById(R.id.dialog_edit_mortgage_balance);

                mortgagePremiumEditText.setText(String.valueOf(mortgageEntry.getAnnualInsurancePremium()));
                mortgageBalanceEditText.setText(String.valueOf(mortgageEntry.getMortgageBalance()));
                mortgageDateEditText.setText(mortgageEntry.getWeeklyIncomeUpdated());
                mortgageIncomeEditText.setText(String.valueOf(mortgageEntry.getCurrentWeeklyIncome()));
                mortgageIssuedEditText.setText(mortgageEntry.getDateMortgageIssued());
                mortgageNameEditText.setText(mortgageEntry.getMortgageeName());
                mortgagePriceEditText.setText(String.valueOf(mortgageEntry.getPrice()));
                mortgageTaxEditText.setText(String.valueOf(mortgageEntry.getAnnualPropertyTax()));


                confirmButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // edit the data and send to server
                        mortgageEntry.setAnnualInsurancePremium(Float.parseFloat(mortgagePremiumEditText.getText().toString()));
                        mortgageEntry.setMortgageBalance(Float.parseFloat(mortgageBalanceEditText.getText().toString()));
                        mortgageEntry.setWeeklyIncomeUpdated(mortgageDateEditText.getText().toString());
                        mortgageEntry.setCurrentWeeklyIncome(Float.valueOf(mortgageIncomeEditText.getText().toString()));
                        mortgageEntry.setDateMortgageIssued(mortgageIssuedEditText.getText().toString());
                        mortgageEntry.setMortgageName(mortgageNameEditText.getText().toString());
                        mortgageEntry.setPrice(Float.parseFloat(mortgagePriceEditText.getText().toString()));
                        mortgageEntry.setAnnualPropertyTax(Float.valueOf(mortgageTaxEditText.getText().toString()));

                        ((MortgageArrayAdapter) mAdapter).notifyDataSetChanged();
                        new InsertMortgageTask().execute(mortgageEntry);
                        new InsertMortgageTask().execute(mortgageEntry);
                        dismiss();
                    }
                });
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
                return view;
            }
        };
        df.show(getFragmentManager(), "");
    }

    private void setToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setIcon(R.mipmap.ic_house);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText(R.string.title_mortgage);

    }

    // Our standard listener to delete multiple items.
    private class MyMultiClickListener implements AbsListView.MultiChoiceModeListener {

        private ArrayList<Mortgage> mMortgagesToDelete = new ArrayList<Mortgage>();

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.context, menu);
            mode.setTitle("Select investments to delete");
            return true; // gives tactile feedback
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.context_delete:
                    deleteSelectedItems();
                    mode.finish();
                    return true;
            }
            return false;
        }

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id,
                                              boolean checked) {


            Mortgage item = (Mortgage) mAdapter.getItem(position);
            if (checked) {

                mMortgagesToDelete.add(item);
            } else {
                mMortgagesToDelete.remove(item);
            }
            mode.setTitle("Selected " + mMortgagesToDelete.size() + " Mortgages");
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            // purposefully empty
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            mMortgagesToDelete = new ArrayList<Mortgage>();
            return true;
        }

        private void deleteSelectedItems() {
            for (Mortgage mortgage : mMortgagesToDelete) {
                ((ArrayAdapter<Mortgage>) mAdapter).remove(mortgage);
                new DeleteMortgageTask().execute(mortgage);
            }
            ((ArrayAdapter<Mortgage>) mAdapter).notifyDataSetChanged();
        }
    }

    class DeleteMortgageTask extends AsyncTask<Mortgage, Void, Void> {

        @Override
        protected Void doInBackground(Mortgage... items) {

            if (!items[0].delete(getFilesDir(), items[0].getAssetNumber())) {
                //TODO not deleted
                Log.d(MY_TAG, "Mortgage not deleted");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            updateMortgages();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mortgage_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_mortgage_add) {
            addMortgage();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void addMortgage() {

        DialogFragment df = new DialogFragment() {
            @Override
            public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                int style = android.support.v4.app.DialogFragment.STYLE_NORMAL, theme = R.style.MyDialogTheme;
                setStyle(style, theme);
            }

            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
                View view = inflater.inflate(R.layout.dialog_add_mortgage, container);
                getDialog().setTitle(R.string.dialog_add_mortgage_title);

                // Change the title and divider
                //http://stackoverflow.com/questions/23255776/android-alert-dialog-replace-default-blue-with-another-color
                //http://stackoverflow.com/questions/23023877/how-to-change-appcompat-dialog-title-and-title-divider-color
                //http://developer.android.com/intl/es/reference/android/app/DialogFragment.html
                final Resources res = getResources();

                final int titleDividerId = res.getIdentifier("titleDivider", "id", "android");
                final int titleId = res.getIdentifier("alertTitle", "id", "android");
                final View titleDivider = getDialog().findViewById(titleDividerId);
                final View title = getDialog().findViewById(titleId);
                if (titleDivider != null) {
                    titleDivider.setBackgroundColor(res.getColor(R.color.accentColor));
                }
                if (title != null) {
                    ((TextView) title).setTextColor(res.getColor(R.color.accentColor));
                }

                final Button confirmButton = (Button) view
                        .findViewById(R.id.dialog_add_mortgage_ok);
                final Button cancelButton = (Button) view
                        .findViewById(R.id.dialog_add_mortgage_cancel);
                final EditText mortgageNumberEditText = (EditText) view
                        .findViewById(R.id.dialog_add_mortgage_number);
                final EditText mortgageNameEditText = (EditText) view
                        .findViewById(R.id.dialog_add_mortgage_name);
                final EditText mortgagePriceEditText = (EditText) view
                        .findViewById(R.id.dialog_add_mortgage_price);
                final EditText mortgageIssuedEditText = (EditText) view
                        .findViewById(R.id.dialog_add_mortgage_issued);
                final EditText mortgageIncomeEditText = (EditText) view
                        .findViewById(R.id.dialog_add_mortgage_income);
                final EditText mortgageDateEditText = (EditText) view
                        .findViewById(R.id.dialog_add_mortgage_date);
                final EditText mortgageTaxEditText = (EditText) view
                        .findViewById(R.id.dialog_add_mortgage_tax);
                final EditText mortgageBalanceEditText = (EditText) view
                        .findViewById(R.id.dialog_add_mortgage_balance);


                confirmButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Mortgage mMortgage = new Mortgage();
                        mMortgage.setAssetNumber(mortgageNumberEditText.getText().toString());
                        //mMortgage.setAnnualInsurancePremium();
                        mMortgage.setAnnualPropertyTax(Float.parseFloat(mortgageTaxEditText.getText().toString()));
                        mMortgage.setCurrentWeeklyIncome(Float.parseFloat(mortgageIncomeEditText.getText().toString()));
                        mMortgage.setDateMortgageIssued(mortgageIssuedEditText.getText().toString());
                        mMortgage.setMortgageBalance(Float.parseFloat(mortgageBalanceEditText.getText().toString()));
                        mMortgage.setMortgageName(mortgageNameEditText.getText().toString());
                        mMortgage.setPrice(Float.parseFloat(mortgagePriceEditText.getText().toString()));
                        mMortgage.setWeeklyIncomeUpdated(mortgageDateEditText.getText().toString());

                        ((MortgageArrayAdapter) mAdapter).add(mMortgage);
                        ((MortgageArrayAdapter) mAdapter).notifyDataSetChanged();

                        Toast.makeText(ManageMortgageActivity.this,
                                "\nThe following record was inserted\n" + mMortgage.print(), Toast.LENGTH_LONG).show();

                        new InsertMortgageTask().execute(mMortgage);

                        dismiss();

                    }
                });

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
                return view;
            }
        };
        df.show(getFragmentManager(), "");


    }

    class InsertMortgageTask extends AsyncTask<Mortgage, Void, Void> {

        @Override
        protected Void doInBackground(Mortgage... items) {
            items[0].save(getFilesDir());
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            updateMortgages();
        }

    }
}
