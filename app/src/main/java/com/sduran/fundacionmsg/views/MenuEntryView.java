package com.sduran.fundacionmsg.views;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sduran.fundacionmsg.R;


public class MenuEntryView extends RelativeLayout {

    private TextView titleTextView;

    public MenuEntryView(Context context) {
        super(context);
        ((Activity) context).getLayoutInflater().inflate(R.layout.menu_entry_view, this);
        titleTextView = (TextView) findViewById(R.id.title_entry_name);
    }

    public void setMenuTitle(String name) {
        titleTextView.setText(name);
    }


}
