package com.sduran.fundacionmsg.adapter;

import java.util.List;


import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.sduran.fundacionmsg.views.MenuEntryView;

public class MenuArrayAdapter extends ArrayAdapter<String> {

	private Context mContext;

	public MenuArrayAdapter(Context context, int resource, String[] titles) {
		super(context, resource, titles);
		mContext = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MenuEntryView view = null;
		if (convertView == null) {
			view = new MenuEntryView(mContext);
		} else {
			view = (MenuEntryView) convertView;
		}
		String title = getItem(position);

		view.setMenuTitle(title);
		return view;
	}

}
