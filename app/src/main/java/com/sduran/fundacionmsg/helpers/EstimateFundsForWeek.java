package com.sduran.fundacionmsg.helpers;

import android.util.Log;

import com.sduran.fundacionmsg.activities.MainActivity;
import com.sduran.fundacionmsg.models.Investment;
import com.sduran.fundacionmsg.models.Mortgage;

import java.io.File;

public class EstimateFundsForWeek {

    public static String compute(File fileDir,float annualOperatingExpenses)
    //
    // computes the estimated funds available for week
    //
    {

        try {
            float expectedWeeklyInvestmentReturn = (float) 0.0; // expected weekly investment return
            float expectedTotalWeeklyNetPayments = (float) 0.0; // expected total mortgage payments less total weekly grants
            float estimatedFunds = (float) 0.0;

            Investment inv = new Investment();    // investment record
            Mortgage mort = new Mortgage();    // mortgage record

            expectedWeeklyInvestmentReturn = inv.totalWeeklyReturnOnInvestment(fileDir);
            expectedTotalWeeklyNetPayments = mort.totalWeeklyNetPayments(fileDir);

            /*estimatedFunds = (expectedWeeklyInvestmentReturn - (MainActivity.getAnnualOperatingExpenses() / (float) 52.0) +
                    expectedTotalWeeklyNetPayments);*/
            estimatedFunds = (expectedWeeklyInvestmentReturn - (annualOperatingExpenses / (float) 52.0) +
                    expectedTotalWeeklyNetPayments);

            Log.d(MainActivity.MY_TAG, "estimatedFunds:" + estimatedFunds);
            MainActivity.setEstimatedFundsForWeek(estimatedFunds);

            return EstimatedFundsReport.printReport();

        } catch (Exception e) {
            return ("***** Error: EstimatedFundsForWeek.compute () *****" + "\n" + "\t" + e);
        }

    }  // compute

}
