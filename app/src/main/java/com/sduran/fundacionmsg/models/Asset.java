
package com.sduran.fundacionmsg.models;

import android.util.Log;

import java.io.*;

public abstract class Asset {

    public static final String MY_TAG = Asset.class.getSimpleName();

    protected String assetNumber;  // unique identifying number of an asset

    // getter-setter methods for Asset
    public String getAssetNumber() {
        return assetNumber;
    }

    public void setAssetNumber(String a) {
        assetNumber = a;
    }

    // virtual methods
    public abstract void read(RandomAccessFile fileName);

    public abstract String print();

    public abstract void save();

    public abstract void write(RandomAccessFile fileName);

    public abstract boolean find(String findRecordID);

    public abstract void obtainNewData();

    public abstract void performDeletion(File fileDir);

    public boolean delete(File fileDir,String mNumber)
    //
    // deletes an Asset
    //
    {
        try {
/*
    char	      c;			    // character entered by user
	String        input;                        // buffer for line of characters
	boolean	      done = false;	            // tells when user is done entering information
	boolean       found = false;	            // tells when an investment has been found
	char          choice;           	    // for storing user's response

	while (!found && !done)
	{
	  System.out.println ("Please enter the number of the " + getClass ().getName () +
	                      " to be deleted (12 digits): ");

          input = mNumber;

	  found = find (input);

	  if (!found)
	  {
	    System.out.println (getClass ().getName () + " " + input.toString () +
				" was not found.");
            System.out.println ("Would you like to enter another " + getClass ().getName ()+ " number?");

            choice = mChoice;

	    if (choice == 'n')
	    {
	      done = true;
	    }
	  }
	}

	if (!found)
	{
          return;
	}
*/

            performDeletion(fileDir);
            Log.d(MY_TAG, "\nThe record has been deleted.");
            return true;
    /*UserInterface.pressEnter();*/
        } catch (Exception e) {
            Log.d(MY_TAG, "***** Error: Asset.delete () *****" + "\t" + e);
            return false;
        }

    }  // delete

//----------------------------------------------------------------------------------------------------------------------------------------------------

    public void add()
    //
    // adds a new investment/mortgage.
    //
    {
        try {
            int c;    // character entered by user

            obtainNewData();
            save();
            System.out.println("\nThe following record was inserted\n");
            print();
	/*UserInterface.pressEnter();*/

        } catch (Exception e) {
            System.out.println("***** Error: Asset.add () *****");
            System.out.println("\t" + e);
        }

    }  // add

}  // class Asset
