package com.sduran.fundacionmsg.adapter;


import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.sduran.fundacionmsg.models.Investment;
import com.sduran.fundacionmsg.views.InvestmentEntryView;
import com.sduran.fundacionmsg.views.MenuEntryView;

import java.util.List;

public class InvestmentArrayAdapter extends ArrayAdapter<Investment> {

    private Context mContext;

    public InvestmentArrayAdapter(Context context, int resource, List<Investment> investments) {
        super(context, resource, investments);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        InvestmentEntryView view = null;
        if (convertView == null) {
            Log.d("ejele","1");
            view = new InvestmentEntryView(mContext);
        } else {
            Log.d("ejele","2");
            view = (InvestmentEntryView) convertView;
        }
        Investment investment = getItem(position);

        view.setInvestmentName(investment.getInvestmentName());
        view.setInvestmentExpectedAnnualReturn(String.valueOf(investment.getExpectedAnnualReturn()));
        view.setInvestmentExpectedAnnualReturnUpdated(investment.getExpectedAnnualReturnUpdated());
        view.setInvestmentNumber(investment.getAssetNumber());
        return view;
    }

}
