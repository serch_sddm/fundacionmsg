package com.sduran.fundacionmsg.activities;

import android.app.DialogFragment;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sduran.fundacionmsg.R;
import com.sduran.fundacionmsg.adapter.InvestmentArrayAdapter;
import com.sduran.fundacionmsg.helpers.InvestmentsReport;
import com.sduran.fundacionmsg.models.Investment;

import java.util.ArrayList;
import java.util.List;

public class ManageInvestmentActivity extends AppCompatActivity {

    public static final String MY_TAG = ManageInvestmentActivity.class.getSimpleName();
    Toolbar mToolbar;
    ListView mListView;
    LinearLayout mLoadingForm;
    TextView mEmpty;
    InvestmentArrayAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_investment);
        setToolbar();


        mLoadingForm = (LinearLayout) findViewById(R.id.empty_form);
        mEmpty = (TextView) findViewById(R.id.no_investments_label);

        mListView = (ListView) findViewById(R.id.lv_investment);
        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListView.setMultiChoiceModeListener(new MyMultiClickListener());
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /**
                 * If they click an investment,
                 */
                Investment investmentEntry = (Investment) mAdapter.getItem(position);
                editInvestmentEntry(investmentEntry);

            }
        });

        updateInvestments();

    }

    private void updateInvestments() {
        mLoadingForm.setVisibility(View.VISIBLE);
        mEmpty.setVisibility(View.GONE);
        mListView.setVisibility(View.GONE);
        new QueryForInvestmentsTask().execute();
    }

    class QueryForInvestmentsTask extends AsyncTask<Void, Void, List<Investment>> {

        @Override
        protected List<Investment> doInBackground(Void... unused) {
            List<Investment> investments = null;
            investments = InvestmentsReport.getReport(getFilesDir());
            return investments;
        }

        @Override
        protected void onPostExecute(List<Investment> result) {
            super.onPostExecute(result);
            if (result == null) {
                mEmpty.setText("Failed loading, result is null");
                return;
            }

            if (result.isEmpty()) {
                Log.d(MY_TAG, "result is empty");
                result = new ArrayList<Investment>();
            }

            mAdapter = new InvestmentArrayAdapter(
                    ManageInvestmentActivity.this, android.R.layout.simple_list_item_activated_1,
                    result);
            mListView.setAdapter(mAdapter);


            mLoadingForm.setVisibility(View.GONE);

            if (mAdapter.isEmpty()) {
                mEmpty.setVisibility(View.VISIBLE);
            } else {
                mListView.setVisibility(View.VISIBLE);
            }

        }
    }

    private void editInvestmentEntry(final Investment investmentEntry) {
        DialogFragment df = new DialogFragment() {
            @Override
            public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                int style = android.support.v4.app.DialogFragment.STYLE_NORMAL, theme = R.style.MyDialogTheme;
                setStyle(style, theme);
            }

            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
                View view = inflater.inflate(R.layout.dialog_edit_investment_entry, container);
                getDialog().setTitle(getString(R.string.dialog_edit_investment_entry_title));

                // Change the title and divider
                //http://stackoverflow.com/questions/23255776/android-alert-dialog-replace-default-blue-with-another-color
                //http://stackoverflow.com/questions/23023877/how-to-change-appcompat-dialog-title-and-title-divider-color
                //http://developer.android.com/intl/es/reference/android/app/DialogFragment.html
                final Resources res = getResources();

                final int titleDividerId = res.getIdentifier("titleDivider", "id", "android");
                final int titleId = res.getIdentifier("alertTitle", "id", "android");
                final View titleDivider = getDialog().findViewById(titleDividerId);
                final View title = getDialog().findViewById(titleId);
                if (titleDivider != null) {
                    titleDivider.setBackgroundColor(res.getColor(R.color.accentColor));
                }
                if (title != null) {
                    ((TextView) title).setTextColor(res.getColor(R.color.accentColor));
                }

                final EditText nameEditText = (EditText) view.findViewById(R.id.dialog_edit_investment_entry_name);
                final EditText returnEditText = (EditText) view.findViewById(R.id.dialog_edit_investment_entry_return);
                final EditText dateEditText = (EditText) view.findViewById(R.id.dialog_edit_investment_entry_date);
                final Button confirmButton = (Button) view.findViewById(R.id.dialog_edit_investment_ok);
                final Button cancelButton = (Button) view.findViewById(R.id.dialog_edit_investment_cancel);

                nameEditText.setText(String.valueOf(investmentEntry.getInvestmentName()));
                returnEditText.setText(String.valueOf(investmentEntry.getExpectedAnnualReturn()));
                dateEditText.setText(String.valueOf(investmentEntry.getExpectedAnnualReturnUpdated()));

                confirmButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // edit the data and send to server
                        investmentEntry.setInvestmentName(nameEditText.getText().toString());
                        investmentEntry.setExpectedAnnualReturn(Float.parseFloat(returnEditText.getText().toString()));

                        ((InvestmentArrayAdapter) mAdapter).notifyDataSetChanged();
                        new InsertInvestmentTask().execute(investmentEntry);
                        dismiss();
                    }
                });
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
                return view;
            }
        };
        df.show(getFragmentManager(), "");
    }

    private void setToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setIcon(R.mipmap.ic_house);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText(R.string.title_investment);

    }

    // Our standard listener to delete multiple items.
    private class MyMultiClickListener implements AbsListView.MultiChoiceModeListener {

        private ArrayList<Investment> mInvestmentsToDelete = new ArrayList<Investment>();

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.context, menu);
            mode.setTitle("Select investments to delete");
            return true; // gives tactile feedback
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.context_delete:
                    deleteSelectedItems();
                    mode.finish();
                    return true;
            }
            return false;
        }

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id,
                                              boolean checked) {


            Investment item = (Investment) mAdapter.getItem(position);
            if (checked) {

                mInvestmentsToDelete.add(item);
            } else {
                mInvestmentsToDelete.remove(item);
            }
            mode.setTitle("Selected " + mInvestmentsToDelete.size() + " Investments");
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            // purposefully empty
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            mInvestmentsToDelete = new ArrayList<Investment>();
            return true;
        }

        private void deleteSelectedItems() {
            for (Investment investment : mInvestmentsToDelete) {
                ((ArrayAdapter<Investment>) mAdapter).remove(investment);
                new DeleteInvestmentTask().execute(investment);
            }
            ((ArrayAdapter<Investment>) mAdapter).notifyDataSetChanged();
        }
    }

    class DeleteInvestmentTask extends AsyncTask<Investment, Void, Void> {

        @Override
        protected Void doInBackground(Investment... items) {

            if (!items[0].delete(getFilesDir(), items[0].getAssetNumber())) {
                //TODO not deleted
                Log.d(MY_TAG,"Investment not deleted");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            updateInvestments();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.investment_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_investment_add) {
            addInvestment();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void addInvestment() {

        DialogFragment df = new DialogFragment() {
            @Override
            public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                int style = android.support.v4.app.DialogFragment.STYLE_NORMAL, theme = R.style.MyDialogTheme;
                setStyle(style, theme);
            }
            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
                View view = inflater.inflate(R.layout.dialog_add_investment, container);
                getDialog().setTitle(R.string.dialog_add_investment_title);

                // Change the title and divider
                //http://stackoverflow.com/questions/23255776/android-alert-dialog-replace-default-blue-with-another-color
                //http://stackoverflow.com/questions/23023877/how-to-change-appcompat-dialog-title-and-title-divider-color
                //http://developer.android.com/intl/es/reference/android/app/DialogFragment.html
                final Resources res = getResources();

                final int titleDividerId = res.getIdentifier("titleDivider", "id", "android");
                final int titleId = res.getIdentifier("alertTitle", "id", "android");
                final View titleDivider = getDialog().findViewById(titleDividerId);
                final View title = getDialog().findViewById(titleId);
                if (titleDivider != null) {
                    titleDivider.setBackgroundColor(res.getColor(R.color.accentColor));
                }
                if (title != null) {
                    ((TextView) title).setTextColor(res.getColor(R.color.accentColor));
                }

                final Button confirmButton = (Button) view
                        .findViewById(R.id.dialog_add_investment_ok);
                final Button cancelButton = (Button) view
                        .findViewById(R.id.dialog_add_investment_cancel);
                final EditText investmentNumberEditText = (EditText) view
                        .findViewById(R.id.dialog_add_investment_number);
                final EditText investmentNameEditText = (EditText) view
                        .findViewById(R.id.dialog_add_investment_name);
                final EditText investmentReturnEditText = (EditText) view
                        .findViewById(R.id.dialog_add_investment_return);
                final EditText investmentDateEditText = (EditText) view
                        .findViewById(R.id.dialog_add_investment_date);

                confirmButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Investment mInvestment = new Investment();
                        mInvestment.setExpectedAnnualReturn((new Float(investmentReturnEditText.getText().toString())).floatValue());
                        mInvestment.setExpectedAnnualReturnUpdated(investmentDateEditText.getText().toString());
                        mInvestment.setInvestmentName(investmentNameEditText.getText().toString());
                        mInvestment.setAssetNumber(investmentNumberEditText.getText().toString());

                        ((InvestmentArrayAdapter) mAdapter).add(mInvestment);
                        ((InvestmentArrayAdapter) mAdapter).notifyDataSetChanged();

                        Toast.makeText(ManageInvestmentActivity.this,
                                "\nThe following record was inserted\n" + mInvestment.print(), Toast.LENGTH_LONG).show();

                        new InsertInvestmentTask().execute(mInvestment);

                        dismiss();

                    }
                });

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
                return view;
            }
        };
        df.show(getFragmentManager(), "");


    }

    class InsertInvestmentTask extends AsyncTask<Investment, Void, Void> {

        @Override
        protected Void doInBackground(Investment... items) {
            items[0].save(getFilesDir());
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            updateInvestments();
        }

    }
}
