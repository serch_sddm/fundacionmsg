package com.sduran.fundacionmsg.views;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sduran.fundacionmsg.R;


public class MortgageEntryView extends RelativeLayout {

    private TextView mortgageNameTextView;
    private TextView mortgageNumberTextView;
    private TextView mortgagePriceTextView;
    private TextView mortgageDateIssuedTextView;
    private TextView mortgageWeeklyIncomeTextView;
    private TextView mortgageTodayDateTextView;
    private TextView mortgageAnnualPropertyTextView;
    private TextView mortgageBalanceTextView;
    private TextView mortgagePremiumTextView;
    public MortgageEntryView(Context context) {
        super(context);
        ((Activity) context).getLayoutInflater().inflate(R.layout.mortgage_entry_view, this);
        mortgageNameTextView = (TextView) findViewById(R.id.mortgage_entry_name);
        mortgageNumberTextView = (TextView) findViewById(R.id.mortgage_entry_id);
        mortgagePriceTextView = (TextView) findViewById(R.id.mortgage_entry_price);
        mortgageAnnualPropertyTextView = (TextView) findViewById(R.id.mortgage_entry_tax);
        mortgageBalanceTextView = (TextView) findViewById(R.id.mortgage_entry_balance);
        mortgageDateIssuedTextView = (TextView) findViewById(R.id.mortgage_entry_date_issued);
        mortgageTodayDateTextView = (TextView) findViewById(R.id.mortgage_entry_date);
        mortgageWeeklyIncomeTextView = (TextView) findViewById(R.id.mortgage_entry_income);
        mortgagePremiumTextView = (TextView) findViewById(R.id.mortgage_entry_premium);


    }

    public void setMortgageName(String name) {
        mortgageNameTextView.setText("Name: " + name);
    }

    public void setMortgageNumber(String number) {
        mortgageNumberTextView.setText("ID: " + number);
    }

    public void setMortgagePrice(String mortgagePrice) {
        mortgagePriceTextView.setText("Price of home: $ " + mortgagePrice);
    }

    public void setMortgageDateIssued(String mortgageDateIssued) {
        mortgageDateIssuedTextView.setText("Date mortgage was issued: " + mortgageDateIssued);
    }

    public void setMortgageWeeklyIncome(String mortgageWeeklyIncome) {
        mortgageWeeklyIncomeTextView.setText("Weekly income: " + mortgageWeeklyIncome);
    }

    public void setMortgageTodayDate(String mortgageTodayDate) {
        mortgageTodayDateTextView.setText("Today's date: " + mortgageTodayDate);
    }

    public void setMortgageAnnualProperty(String mortgageAnnualProperty) {
        mortgageAnnualPropertyTextView.setText("Annual property tax: " + mortgageAnnualProperty);
    }

    public void setMortgageBalance(String mortgageBalance) {
        mortgageBalanceTextView.setText("Mortgage balance: " + mortgageBalance);
    }
    public void setMortgagePremium(String premium) {
        mortgagePremiumTextView.setText("Annual Insurance Premium: " + premium);
    }

}
