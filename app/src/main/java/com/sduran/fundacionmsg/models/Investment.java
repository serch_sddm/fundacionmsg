package com.sduran.fundacionmsg.models;

import android.util.Log;

import com.sduran.fundacionmsg.activities.MainActivity;
import com.sduran.fundacionmsg.helpers.InvestmentsReport;

import java.io.*;

public class Investment extends Asset {

    public static final String MY_TAG = Investment.class.getSimpleName();

    private String investmentName;            // investment name
    private float expectedAnnualReturn;    // expected annual return on investment
    private String expectedAnnualReturnUpdated;   // date expected annual return updated

    // getter-setter methods for Investment
    public String getInvestmentName() {
        return investmentName;
    }

    public void setInvestmentName(String n) {
        investmentName = n;
    }

    public float getExpectedAnnualReturn() {
        return expectedAnnualReturn;
    }

    public void setExpectedAnnualReturn(float r) {
        expectedAnnualReturn = r;
    }

    public String getExpectedAnnualReturnUpdated() {
        return expectedAnnualReturnUpdated;
    }

    public void setExpectedAnnualReturnUpdated(String d) {
        expectedAnnualReturnUpdated = d;
    }

    public float totalWeeklyReturnOnInvestment(File fileDir)
    //
    // totalWeeklyReturnOnInvestment reads records from the investments
    // file and computes totalAnnualReturn by summing each expectedAnnualReturn
    // on investments and dividing by WEEKS_IN_YEAR.
    //
    {
        try {
            File investmentFile = new File(fileDir,"investment.dat");
            float totalAnnualReturn = (float) 0.0;    // total annual return on investments

            if (investmentFile.exists()) {
                RandomAccessFile inFile = new RandomAccessFile(investmentFile, "r");

                while (inFile.getFilePointer() != inFile.length()) {
                    read(inFile);
                    totalAnnualReturn += expectedAnnualReturn;
                }

                inFile.close();
            }

            return (totalAnnualReturn / (float) 52.0);
        } catch (Exception e) {
            System.out.println("***** Error: Investment.totalWeeklyReturnOnInvestment () *****");
            System.out.println("***** Returned a value of 0 *****");
            System.out.println("\t" + e);
            return (float) 0.0;
        }

    }  // totalWeeklyReturnOnInvestment

//----------------------------------------------------------------------------------------------------------------------------------------------------

    public boolean find(String findInvestmentID)
    //
    // find locates a given investment record if it exists.
    // Returns true if the investment is located, otherwise false.
    //
    {
        try {
            File investmentFile = new File("investment.dat");
            boolean found = false;        // result of comparison

            if (investmentFile.exists()) {
                RandomAccessFile inFile = new RandomAccessFile(investmentFile, "r");

                while (!found && (inFile.getFilePointer() != inFile.length())) {
                    read(inFile);

                    if (assetNumber.compareTo(findInvestmentID) == 0) found = true;
                }
                inFile.close();
            }

            return found;
        } catch (Exception e) {
            System.out.println("***** Error: Investment.find () *****");
            System.out.println("\t" + e);

            return false;
        }

    }  // find

//----------------------------------------------------------------------------------------------------------------------------------------------------

    public void read(RandomAccessFile fileName)
    //
    // reads an investment record from fileName.
    // Assumes that the existence of fileName has already been established.
    //
    {
        try {
            String inputString = new String();    // for storing investment record
            int i = 0;                        // position in record

            inputString = fileName.readLine();

            StringBuffer input = new StringBuffer();    // for storing field within record

            while (inputString.charAt(i) != '|') {
                input.append(inputString.charAt(i));
                i++;
            }

            assetNumber = input.toString();
            Log.d(MY_TAG,"assetNumber:"+assetNumber);
            i++;

            input = new StringBuffer();
            while (inputString.charAt(i) != '|') {
                input.append(inputString.charAt(i));
                i++;
            }
            investmentName = input.toString();
            i++;

            input = new StringBuffer();
            while (inputString.charAt(i) != '|') {
                input.append(inputString.charAt(i));
                i++;
            }
            Float tempFloat = new Float(input.toString());
            expectedAnnualReturn = tempFloat.floatValue();
            i++;

            input = new StringBuffer();
            while (i < inputString.length()) {
                input.append(inputString.charAt(i));
                i++;
            }
            expectedAnnualReturnUpdated = input.toString();

        } catch (Exception e) {
            Log.d(MY_TAG, "***** Error: Investment.read () *****" + "\t" + e);
        }

    }  // read

//----------------------------------------------------------------------------------------------------------------------------------------------------

    public void write(RandomAccessFile fileName)
    //
    // writes an investment record to fileName.
    //
    {
        try {
            fileName.writeBytes(assetNumber + "|" + investmentName + "|");
            fileName.writeBytes(expectedAnnualReturn + "|");
            fileName.writeBytes(expectedAnnualReturnUpdated.toString() + "\n");
        } catch (Exception e) {
            Log.d(MY_TAG, "***** Error: Investment.write () *****" + "\t" + e);
        }

    }  // write

//----------------------------------------------------------------------------------------------------------------------------------------------------

    public String print()
    //
    // displays the contents of an investment object.
    //
    {
        final StringBuilder mString = new StringBuilder();
        mString.append("Investment ID: " + assetNumber);
        mString.append("\nInvestment Name: " + investmentName);
        mString.append("\nExpected return: " + expectedAnnualReturn);
        mString.append("\nDate expected return was updated: " + expectedAnnualReturnUpdated);
        return mString.toString();
    }  // print


//----------------------------------------------------------------------------------------------------------------------------------------------------

    public static void printAll()
    //
    // displays a list of all investment objects.
    //
    {
        InvestmentsReport.printReport();

    }  // printAll

//----------------------------------------------------------------------------------------------------------------------------------------------------

    @Override
    public void obtainNewData() {

    }

    public void obtainNewData(String mAssetNumber, String mInvestmentName, String mExpectedAnnualReturn, String mExpectedAnnualReturnUpdated)
    //
    // Reads a new investment record by calling readInvestmentData.
    //
    {
        readInvestmentData(mAssetNumber, mInvestmentName, mExpectedAnnualReturnUpdated, mExpectedAnnualReturnUpdated);
    }  // obtainNewData

//----------------------------------------------------------------------------------------------------------------------------------------------------

    public void performDeletion(File fileDir)
    //
    // performDeletion performs the actual deletion of an investment record from a file.
    //
    {
        Log.d(MY_TAG, "Perform deletion");
        try {
            File investmentFile = new File(fileDir,"investment.dat");
            File tempInvestmentFile = new File(fileDir,"investment.tmp");

            Investment investment = new Investment();    // record to be checked

            if (!investmentFile.exists()) {
                return;
            }

            RandomAccessFile inFile = new RandomAccessFile(investmentFile, "r");
            RandomAccessFile outFile = new RandomAccessFile(tempInvestmentFile, "rw");

            while (inFile.getFilePointer() != inFile.length()) {
                investment.read(inFile);

                if (assetNumber.compareTo(investment.getAssetNumber()) != 0) {
                    investment.write(outFile);
                }
            }

            inFile.close();
            outFile.close();

            investmentFile.delete();
            tempInvestmentFile.renameTo(investmentFile);

        } catch (Exception e) {
            System.out.println("***** Error: Investment.performDeletion () *****");
            System.out.println("\t" + e);
        }

    }  // performDeletion

//----------------------------------------------------------------------------------------------------------------------------------------------------

    @Override
    public void save() {

    }

    public void save(File fileDir)
    //
    // saves an individual investment record into a file, ordered by investmentID.
    //
    {
        try {
            Log.d(MY_TAG, "Enter try");
            File investmentFile = new File(fileDir, "investment.dat");    // file of investments
            File tempInvestmentFile = new File(fileDir, "investment.tmp");    // temporary file for investments

            Investment investment = new Investment();    // record read, then written
            boolean found = false;        // terminates while-loop

            RandomAccessFile newFile = new RandomAccessFile(tempInvestmentFile, "rw");

            if (!investmentFile.exists()) {
                Log.d(MY_TAG, "no exists");
                write(newFile);
            } else {
                Log.d(MY_TAG, "exists");
                RandomAccessFile oldFile = new RandomAccessFile(investmentFile, "r");

                int compareInvestments; // to find correct place for the new investment

                while (oldFile.getFilePointer() != oldFile.length()) {
                    investment.read(oldFile);

                    compareInvestments = assetNumber.compareTo(investment.getAssetNumber());

                    if ((!found) && (compareInvestments < 0)) {
                        write(newFile);
                        investment.write(newFile);
                        found = true;
                    } else if (compareInvestments == 0) {
                        write(newFile);
                        found = true;
                    } else {
                        investment.write(newFile);
                    }
                }  // while

                if (!found) write(newFile);

                oldFile.close();

            }  // else

            newFile.close();

            investmentFile.delete();
            tempInvestmentFile.renameTo(investmentFile);

        } catch (Exception e) {
            System.out.println("***** Error: Investment.putRecord () *****");
            System.out.println("\t" + e);
        }

    }  // save

//----------------------------------------------------------------------------------------------------------------------------------------------------

    public void readInvestmentData(String mAssetNumber, String mInvestmentName, String mExpectedAnnualReturn, String mExpectedAnnualReturnUpdated)
    //
    // readInvestmentData obtains input data for all fields of an investment record.
    //
    {
        try {
            char c;                // character entered by user
            String input;                        // buffer for line of characters
            boolean valid = false;            // used to validate length of ID

            while (!valid) {
                System.out.println("Enter investment number (12 digits): ");
                assetNumber = mAssetNumber;
                valid = (assetNumber.length() <= 12);

                if (!valid)
                    System.out.println("\n\nThe investment number must be 12 digits long.");
            }

            System.out.println("Enter investment name: ");
            investmentName = mInvestmentName;

            System.out.println("Enter expected annual return: ");
            input = mExpectedAnnualReturn;
            Float newFloat = new Float(input);
            expectedAnnualReturn = newFloat.floatValue();

            System.out.println("Enter today's date (mm/dd/yy):");
            expectedAnnualReturnUpdated = mExpectedAnnualReturnUpdated;

        } catch (Exception e) {
            System.out.println("***** Error: Investment.readInvestmentData () *****");
            System.out.println("\t" + e);
        }

    }  // readInvestmentData

//----------------------------------------------------------------------------------------------------------------------------------------------------

    public void updateInvestmentName(String mInvestmentName) {

        System.out.println("\n\nOld investment name: " + investmentName);
        System.out.println("Enter new investment name: ");
        investmentName = mInvestmentName;

    } // updateInvestmentName

//----------------------------------------------------------------------------------------------------------------------------------------------------

    public void updateExpectedReturn(String mExpectedAnnualReturn, String mExpectedAnnualReturnUpdated) {

        String input;

        System.out.println("\n\nOld expected return: " + expectedAnnualReturn);
        System.out.println("Enter new expected return: ");
        input = mExpectedAnnualReturn;
        Float tempFloat = new Float(input);
        expectedAnnualReturn = tempFloat.floatValue();

        System.out.println("Enter today's date (mm/dd/yy):");
        expectedAnnualReturnUpdated = mExpectedAnnualReturnUpdated;

    } // updateExpectedReturn

}  // class Investment
