package com.sduran.fundacionmsg.helpers;

import android.util.Log;

import com.sduran.fundacionmsg.activities.MainActivity;

import java.io.*;

public class EstimatedFundsReport {

    public static String printReport()
    //
    // computes total funds available for purchasing new mortgages in the current week.
    //
    {
        String result = "";
        //result = "Funds available: ";
        result= result+MainActivity.getEstimatedFundsForWeek();
        return result;
    }  // printReport

}
