package com.sduran.fundacionmsg.views;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sduran.fundacionmsg.R;


public class InvestmentEntryView extends RelativeLayout {

    private TextView investmentNameTextView;
    private TextView investmentExpectedAnnualReturnTextView;
    private TextView investmentExpectedAnnualReturnUpdatedTextView;
    private TextView investmentNumberTextView;

    public InvestmentEntryView(Context context) {
        super(context);
        ((Activity) context).getLayoutInflater().inflate(R.layout.investment_entry_view, this);
        investmentNameTextView = (TextView) findViewById(R.id.investment_entry_name);
        investmentExpectedAnnualReturnTextView = (TextView) findViewById(R.id.investment_entry_return);
        investmentExpectedAnnualReturnUpdatedTextView = (TextView) findViewById(R.id.investment_entry_date);
        investmentNumberTextView = (TextView) findViewById(R.id.investment_entry_id);

    }

    public void setInvestmentName(String name) {
        investmentNameTextView.setText("Name: "+name);
    }

    public void setInvestmentExpectedAnnualReturn(String expectedAnnualReturn) {
        investmentExpectedAnnualReturnTextView.setText("Expected return: $"+expectedAnnualReturn);
    }

    public void setInvestmentExpectedAnnualReturnUpdated(String expectedAnnualReturnUpdated) {
        investmentExpectedAnnualReturnUpdatedTextView.setText("Return was updated: "+expectedAnnualReturnUpdated);
    }

    public void setInvestmentNumber(String number) {
        investmentNumberTextView.setText("ID: "+number);
    }


}
