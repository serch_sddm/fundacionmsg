package com.sduran.fundacionmsg.adapter;


import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.sduran.fundacionmsg.models.Investment;
import com.sduran.fundacionmsg.models.Mortgage;
import com.sduran.fundacionmsg.views.InvestmentEntryView;
import com.sduran.fundacionmsg.views.MortgageEntryView;

import java.util.List;

public class MortgageArrayAdapter extends ArrayAdapter<Mortgage> {

    private Context mContext;

    public MortgageArrayAdapter(Context context, int resource, List<Mortgage> mortgages) {
        super(context, resource, mortgages);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MortgageEntryView view = null;
        if (convertView == null) {
            Log.d("ejele", "1");
            view = new MortgageEntryView(mContext);
        } else {
            Log.d("ejele","2");
            view = (MortgageEntryView) convertView;
        }
        Mortgage mortgage = getItem(position);

        view.setMortgageAnnualProperty(String.valueOf(mortgage.getAnnualPropertyTax()));
        view.setMortgageBalance(String.valueOf(mortgage.getMortgageBalance()));
        view.setMortgageDateIssued(mortgage.getDateMortgageIssued());
        view.setMortgageName(mortgage.getMortgageeName());
        view.setMortgageNumber(mortgage.getAssetNumber());
        view.setMortgagePrice(String.valueOf(mortgage.getPrice()));
        view.setMortgageWeeklyIncome(String.valueOf(mortgage.getCurrentWeeklyIncome()));
        view.setMortgageTodayDate(mortgage.getWeeklyIncomeUpdated());
        view.setMortgagePremium(String.valueOf(mortgage.getAnnualInsurancePremium()));

        return view;
    }

}
