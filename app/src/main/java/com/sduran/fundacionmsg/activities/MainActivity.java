package com.sduran.fundacionmsg.activities;


import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.sduran.fundacionmsg.R;
import com.sduran.fundacionmsg.adapter.MenuArrayAdapter;
import com.sduran.fundacionmsg.helpers.EstimateFundsForWeek;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.RandomAccessFile;

public class MainActivity extends AppCompatActivity {

    public static final String MY_TAG = MainActivity.class.getSimpleName();
    Toolbar mToolbar;
    ListView mMenu;

    MenuArrayAdapter mAdapter;

    String[] items;

    private static float estimatedAnnualOperatingExpenses;    // annual operating expenses
    private static float estimatedFundsForWeek;                   // weekly funds available

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setToolbar();

        initializeApplication();

        mMenu = (ListView) findViewById(R.id.lv_main_menu);

        items = new String[]{
                getString(R.string.main_menu_item_1),
                getString(R.string.main_menu_item_2),
                getString(R.string.main_menu_item_3),
                getString(R.string.main_menu_item_4),
                //getString(R.string.main_menu_item_5)
        };

        mAdapter = new MenuArrayAdapter(this, android.R.layout.simple_list_item_activated_1, items);
        mMenu.setAdapter(mAdapter);
        mMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        Log.d(MY_TAG, "position 0:" + position);
                        getFoundsForWeek();
                        break;
                    case 1:
                        Log.d(MY_TAG, "position 1:" + position);
                        manageInvestment();
                        break;
                    case 2:
                        Log.d(MY_TAG, "position 2:" + position);
                        manageMortgage();
                        break;
                    case 3:
                        Log.d(MY_TAG, "position 3:" + position);
                        updateOperatingExpenses();
                        break;
                    /*case 4:
                        Log.d(MY_TAG, "position 4:" + position);
                        produceReports();
                        break;*/
                    default:
                        break;
                }
            }
        });
    }

    private void setToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setIcon(R.mipmap.ic_house);
        ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText(R.string.title_menu);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initializeApplication()
    //
    // initializeApplication initializes annual operating expenses from file opexp.dat, if it exists.
    // If file does not exist, annual operating expenses are set to 0.0 by calling
    // setAnnualOperatingExpenses
    //
    {
        try {
            File operatingExpensesFile = new File(getFilesDir(), "opexp.dat");    // file containing annual operating expenses

            if (!operatingExpensesFile.exists()) {
                Log.d(MY_TAG, "enter no exists");
                setAnnualOperatingExpenses((float) 0.0);
            } else {
                Log.d(MY_TAG, "enter exists");
                RandomAccessFile inFile = new RandomAccessFile(operatingExpensesFile, "r");
                char c;                // character entered by user


                DialogFragment df = new DialogFragment() {

                    @Override
                    public void onCreate(Bundle savedInstanceState) {
                        super.onCreate(savedInstanceState);
                        int style = DialogFragment.STYLE_NORMAL, theme = R.style.MyDialogTheme;
                        setStyle(style, theme);
                    }

                    @Override
                    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                             Bundle savedInstanceState) {
                        View view = inflater.inflate(R.layout.dialog_add_expenses, container);
                        getDialog().setTitle(getString(R.string.dialog_expenses_title));

                        // Change the title and divider
                        //http://stackoverflow.com/questions/23255776/android-alert-dialog-replace-default-blue-with-another-color
                        //http://stackoverflow.com/questions/23023877/how-to-change-appcompat-dialog-title-and-title-divider-color
                        //http://developer.android.com/intl/es/reference/android/app/DialogFragment.html
                        final Resources res = getResources();

                        final int titleDividerId = res.getIdentifier("titleDivider", "id", "android");
                        final int titleId = res.getIdentifier("alertTitle", "id", "android");
                        final View titleDivider = getDialog().findViewById(titleDividerId);
                        final View title = getDialog().findViewById(titleId);
                        if (titleDivider != null) {
                            titleDivider.setBackgroundColor(res.getColor(R.color.accentColor));
                        }
                        if (title != null) {
                            ((TextView) title).setTextColor(res.getColor(R.color.accentColor));
                        }

                        final Button confirmButton = (Button) view
                                .findViewById(R.id.dialog_expenses_ok);
                        final EditText expensesText = (EditText) view
                                .findViewById(R.id.dialog_expenses_amount);


                        expensesText.append(String.valueOf(getAnnualOperatingExpenses()));
                        confirmButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                        /*Toast.makeText(AssignmentListActivity.this,
                                "Got the assignment named " + name, Toast.LENGTH_LONG).show();*/
                                // add the data and send to server
                                String input;            // buffer for line of characters
                                input = expensesText.getText().toString();
                                Float tempFloat = new Float(input.toString());
                                estimatedAnnualOperatingExpenses = tempFloat.floatValue();
                                setAnnualOperatingExpenses(tempFloat.floatValue());
                                dismiss();
                            }
                        });
                        return view;
                    }
                };
                df.show(getSupportFragmentManager(), "");


                inFile.close();
            }
        } catch (Exception e) {
            Log.d(MY_TAG, "***** Error: MSGApplication.initializeApplication () *****" + "\t" + e);
        }

    }  // initializeApplication ()

    public void setAnnualOperatingExpenses(float newAnnualOperatingExpenses)
    //
    // Validates new annual operating expenses, writes data to opexp.dat
    // for use in the current execution.
    //
    {

        try {
            if (newAnnualOperatingExpenses >= 0.0) {
                Log.d(MY_TAG, "***** Enter if *****");
                Log.d(MY_TAG, "newAnnualOperatingExpenses:" + newAnnualOperatingExpenses);
                File operatingExpensesFile = new File(getFilesDir(), "opexp.dat");
                Log.d(MY_TAG, "getFilesDir().path:" + operatingExpensesFile.getPath());// file containing annual operating expenses
                FileOutputStream newFileOutput = new FileOutputStream(operatingExpensesFile);  // output stream for newPrintStream
                PrintStream newFilePrint = new PrintStream(newFileOutput);                      // to output newAnnualOperatingExpenses

                newFilePrint.println(newAnnualOperatingExpenses);
                newFilePrint.close();
                newFileOutput.close();

                estimatedAnnualOperatingExpenses = newAnnualOperatingExpenses;

                if (!operatingExpensesFile.exists())
                    Log.d(MY_TAG, "enter no exists 2");
                else
                    Log.d(MY_TAG, "enter exists 2");
            }
        } catch (Exception e) {
            Log.d(MY_TAG, "***** Error: MSGApplication.setAnnualOperatingExpenses () *****" + "\t" + e);
        }

    }  // setAnnualOperatingExpenses ()

    public float getFileAnnualOperatingExpenses()
    //
    // Validates new annual operating expenses, writes data to opexp.dat
    // for use in the current execution.
    //http://javarevisited.blogspot.mx/2012/07/read-file-line-by-line-java-example-scanner.html
    {

        try {

            float newAnnualOperatingExpenses;

            File operatingExpensesFile = new File(getFilesDir(), "opexp.dat");
            FileInputStream newFileInput = new FileInputStream(operatingExpensesFile);  // output stream for newPrintStream

            BufferedReader mReader = new BufferedReader(new InputStreamReader(newFileInput));

            String line = mReader.readLine();
            if (!line.isEmpty()) {
                newAnnualOperatingExpenses = Float.parseFloat(line);
                Log.d(MY_TAG, "newAnnualOperatingExpenses:" + newAnnualOperatingExpenses);

            } else {
                newAnnualOperatingExpenses = 0;
            }

            mReader.close();
            newFileInput.close();
            return newAnnualOperatingExpenses;

        } catch (Exception e) {
            Log.d(MY_TAG, "***** Error: MSGApplication.getFileAnnualOperatingExpenses () *****" + "\t" + e);
            return 0;
        }

    }  // setAnnualOperatingExpenses ()


    private void produceReports() {
    }

    private void updateOperatingExpenses() {

        updateAnnualOperatingExpenses();

    }

    private void manageMortgage() {
        Intent mIntent = new Intent(MainActivity.this, ManageMortgageActivity.class);
        startActivity(mIntent);
    }

    private void manageInvestment() {

        Intent mIntent = new Intent(MainActivity.this, ManageInvestmentActivity.class);
        startActivity(mIntent);
    }

    public void updateAnnualOperatingExpenses()
    //
    // updateAnnualOperatingExpenses obtains the new annual operating expenses from user, validates,
    // and then writes the data to the operating expenses file.
    //
    {
        try {

            DialogFragment df = new DialogFragment() {

                @Override
                public void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    int style = DialogFragment.STYLE_NORMAL, theme = R.style.MyDialogTheme;
                    setStyle(style, theme);
                }

                @Override
                public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                         Bundle savedInstanceState) {
                    View view = inflater.inflate(R.layout.dialog_add_expenses, container);
                    getDialog().setTitle(getString(R.string.dialog_expenses_title));

                    // Change the title and divider
                    //http://stackoverflow.com/questions/23255776/android-alert-dialog-replace-default-blue-with-another-color
                    //http://stackoverflow.com/questions/23023877/how-to-change-appcompat-dialog-title-and-title-divider-color
                    //http://developer.android.com/intl/es/reference/android/app/DialogFragment.html
                    final Resources res = getResources();

                    final int titleDividerId = res.getIdentifier("titleDivider", "id", "android");
                    final int titleId = res.getIdentifier("alertTitle", "id", "android");
                    final View titleDivider = getDialog().findViewById(titleDividerId);
                    final View title = getDialog().findViewById(titleId);
                    if (titleDivider != null) {
                        titleDivider.setBackgroundColor(res.getColor(R.color.accentColor));
                    }
                    if (title != null) {
                        ((TextView) title).setTextColor(res.getColor(R.color.accentColor));
                    }

                    final TextView mSubtitle = (TextView) view
                            .findViewById(R.id.subtitle);
                    final Button confirmButton = (Button) view
                            .findViewById(R.id.dialog_expenses_ok);
                    final EditText expensesText = (EditText) view
                            .findViewById(R.id.dialog_expenses_amount);


                    mSubtitle.setText("Update the estimated operating expenses ($):");
                    expensesText.append(String.valueOf(getAnnualOperatingExpenses()));
                    confirmButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        /*Toast.makeText(AssignmentListActivity.this,
                                "Got the assignment named " + name, Toast.LENGTH_LONG).show();*/
                            // add the data and send to server
                            String input;            // buffer for line of characters
                            input = expensesText.getText().toString();
                            Float tempFloat = new Float(input.toString());
                            estimatedAnnualOperatingExpenses = tempFloat.floatValue();
                            setAnnualOperatingExpenses(tempFloat.floatValue());
                            dismiss();
                        }
                    });
                    return view;
                }
            };
            df.show(getSupportFragmentManager(), "");


        } catch (Exception e) {
            Log.d(MY_TAG, "***** Error: MSGApplication.updateAnnualOperatingExpenses () *****" + "\t" + e);
        }

    }  // updateAnnualOperatingExpenses ()


    private void getFoundsForWeek() {
        DialogFragment df = new DialogFragment() {

            @Override
            public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                int style = DialogFragment.STYLE_NORMAL, theme = R.style.MyDialogTheme;
                setStyle(style, theme);
            }

            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
                View view = inflater.inflate(R.layout.dialog_show_founds, container);
                getDialog().setTitle(getString(R.string.dialog_founds_title));

                // Change the title and divider
                //http://stackoverflow.com/questions/23255776/android-alert-dialog-replace-default-blue-with-another-color
                //http://stackoverflow.com/questions/23023877/how-to-change-appcompat-dialog-title-and-title-divider-color
                //http://developer.android.com/intl/es/reference/android/app/DialogFragment.html
                final Resources res = getResources();

                final int titleDividerId = res.getIdentifier("titleDivider", "id", "android");
                final int titleId = res.getIdentifier("alertTitle", "id", "android");
                final View titleDivider = getDialog().findViewById(titleDividerId);
                final View title = getDialog().findViewById(titleId);
                if (titleDivider != null) {
                    titleDivider.setBackgroundColor(res.getColor(R.color.accentColor));
                }
                if (title != null) {
                    ((TextView) title).setTextColor(res.getColor(R.color.accentColor));
                }

                final Button confirmButton = (Button) view
                        .findViewById(R.id.dialog_founds_ok);
                final TextView foundsAmountText = (TextView) view
                        .findViewById(R.id.dialog_founds_amount);

                foundsAmountText.append(EstimateFundsForWeek.compute(getFilesDir(),getAnnualOperatingExpenses()));

                confirmButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /*Toast.makeText(AssignmentListActivity.this,
                                "Got the assignment named " + name, Toast.LENGTH_LONG).show();*/
                        // add the data and send to server
                        dismiss();
                    }
                });
                return view;
            }
        };
        df.show(getSupportFragmentManager(), "");
    }

    public static void setEstimatedFundsForWeek(float e)
    //
    //  display report for each change of estimate
    //
    {
        estimatedFundsForWeek = e;
    }

    // getter-setters for estimatedFundsForWeek
    public static float getEstimatedFundsForWeek() {
        Log.d(MY_TAG, "estimatedFundsForWeek:" + estimatedFundsForWeek);
        return estimatedFundsForWeek;
    }

    // getter for annualOperatingExpenses
    public float getAnnualOperatingExpenses() {
        Log.d(MY_TAG, "before estimatedAnnualOperatingExpenses:" + estimatedAnnualOperatingExpenses);
        estimatedAnnualOperatingExpenses = getFileAnnualOperatingExpenses();
        Log.d(MY_TAG, "after estimatedAnnualOperatingExpenses:" + estimatedAnnualOperatingExpenses);
        return estimatedAnnualOperatingExpenses;
    }
}
